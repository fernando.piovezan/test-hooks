CHANGELOG
----------------------

[Novidades](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commits/develop)

[v0.1.9]
 * [feat(VMAGROERP-123): teste30](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/a8347ed05e7298ef9c2a4b28933f8e2a375b4cf4)

[v0.1.8]
 * [feat(VMAGROERP-123): teste29](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/4458394804d5cc39b5b594944928d953dd825708)
 * [feat(VMAGROERP-123): teste28](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/175c0d2e82952dd8ebb663345b15fbaa1d5f1abc)
 * [feat(VMAGROERP-123): teste27](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/3344bf892421193ee38d827dfbd3da6ad8c6ae31)

[v0.1.7]
 * [feat(VMAGROERP-123): teste26](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/50ad9b6cbd6727b47b18c670899dde9cacdf2308)
 * [feat(VMAGROERP-123): teste25](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/f477e8c04507fdc346a37c0f3a0128b03dd3b8e5)
 * [feat(VMAGROERP-123): teste23](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/6dbdc092974c69479e674ddf0c31ba9c9de25f29)
 * [feat(VMAGROERP-123): teste22](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/b003ce18d1061832bbb3aa59ff38951e233f1227)
 * [feat(VMAGROERP-123): teste22](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/eef736dcbada6175137c81ce4067831e1c7399cc)
 * [feat(VMAGROERP-123): teste3](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/e8e73222cea1d88a8e620c20455d787cd9b943fe)
 * [feat(VMAGROERP-123): teste3](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/76f578ac0ed8c0837c44ea78ba2a7023177ff2bd)
 * [feat(VMAGROERP-123): teste3](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/685a2a389c0297df99b3400ffa48987d8bb2a255)
 * [feat(VMAGROERP-123): teste3](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/9703776bcefd61d17a5fae96a2c344bd7f972af1)
 * [feat(VMAGROERP-123): teste3](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/7b4d1c677786db52a1d3dcf3f05a3fb5bf9d429d)
 * [feat(VMAGROERP-123): teste3](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/9d5500802a006f8ac3bb409f9197e9dfeac94137)

[v0.1.6]
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/ca46e8e4ea14574a2bab28d72e0fe9a1f5d1768f)
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/253a137c0838e30f5e07d1e22bb75b7c13c6d794)
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/014567061ce4db419d551464f3a919147de6264b)

[v0.1.5]
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/d504ca795ca3e77174f28ee9d8c603019cb1c42e)

[v0.1.4]
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/6fcb5b9dcdbe013f05c232760bbd8c3ad4f2e255)
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/d8ed49ff8704190edae08cb04b18b628afc7a1ac)

[v0.1.3]
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/59ec9c22bde80f8d7824cc1e0f1fb47e4147505a)

[v0.1.2]

[v0.1.1]
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/047636a996243e1912ee860453feb1a7b366017d)

[v0.1.0](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commits/v0.1.0)
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/d236b76429e51b0ebdc23afd728058cc2115860e)
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/5211765b1394d6c82d5cb765fc1532d637e7cb02)
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/7d13d2f06d981ce7a4fc0e0c583f9e333e18b631)
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/269211d68b8d705c5d56d8aaddb580a7406b945d)
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/9fd3e52a6a5141afa2d15e3d790d731d97d9cf9d)
 * [feat(VMAGROERP-123): teste de hook](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/114cdb9a4a2e44c234d78120d741f592c06d8dd9)
 * [Initial commit](https://gitlab.com/Visionnaire/vmagro/vmagro-erpcolheitas-outsourcing/-/commit/20200210f5c252f2ca7e41e4ae9a7c7e4aa50993)
